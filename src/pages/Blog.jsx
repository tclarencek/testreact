import React from 'react';
import {withRouter} from 'react-router-dom';
import PostPreview from '../components/PostPreview';
import Header from "../components/Header";
import {get,url} from '../utils/Network';
class Blog extends React.Component{
    state={loading:true,content:[]};
    componentDidMount(){
        if(this.props.type===1){
            get(`/category/${this.props.match.params.id}/posts`,{},(res)=>{
                this.setState({content:res.data});
            },()=>{
                this.setState({loading:false})
            })
        }
        if(this.props.type===0){
            get(`/post/all/list`,{},(res)=>{
                this.setState({content:res.data});
            },()=>{
                this.setState({loading:false})
            })
        }
    }
    render(){
        return(
            <div>
                <Header content={["Posts found"]} context={this}/>
                <br/>

                <div className="container">
                    <br/>
                    <center><h1>{this.state.content.length} Total</h1>
                        <h5>Post(s) following your criteria</h5></center>
                    <br/>
                    <h6>Resides below</h6>
                    <hr style={{marginBottom:0}}/>
                    {!this.state.loading && this.state.content &&
                    <div className="row">
                        {this.state.content.map((elt,i)=>{

                                return(
                                    <div className="col-sm-12 col-md-4">
                                        <PostPreview title={elt.title} key={i} author={elt.userId} excerpt={elt.excerpt} image={elt.postImage?elt.postImage:"https://www.pexels.com/photo/4458/download/?search_query=blog&tracking_id=yg0e5xhzzf"} onClick={()=>{
                                            this.props.history.push('/article/'+elt._id)
                                        }}/>
                                    </div>
                                )

                        })}
                    </div>
                    }
                </div>
            </div>
        );
    }
}
export default withRouter(Blog);