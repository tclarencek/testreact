import React from "react";
import Typed from "react-typed";
import './welcome.css';
import ActionButton from "../components/ActionButton";
import {get} from '../utils/Network';
import {withRouter} from 'react-router-dom'
class Welcome extends React.Component{
    constructor(props) {
        super(props);
        this.state={name:'',email:'',password:'',loadingLogin:false,login:true};
    }
    render() {
        return (
            <div style={{height:"100%",width:"100%"}}>
                <div className="bgimg"/>
                <div className="bgColor row" style={{margin:0}}>
                    <div className="col-md-5 col-sm-12">
                        <div className="" style={{padding:60}}>
                            <div>
                                {/*<h1 className="sketch" style={{color:"white",fontSize:"5em",textStyle:"italic"}}>Ze Blog</h1>*/}
                                <div className="white-section-cover">
                                    <div className="white-section">

                                        <Typed
                                            strings={['Hey There!<br/>This is the blog','Hope you like it','Login/Sign Up or view blog']}
                                            typeSpeed={40}
                                            className="main_text"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-7 col-sm-12" style={{background:"white",height:"100%"}}>
                        <div className="container d-flex flex-column justify-content-center align-content-center h-100">
                            <div className="login-signup-content">
                                {this.state.login && <div className="login-content">
                                    <h1 className="sketch" style={{color:"black",fontSize:"4em",textStyle:"italic"}}>Login</h1>
                                    <br/>
                                    <input type="text" className="form-control" name="email" style={{height: "2.7rem"}} placeholder="Your email here" onChange={e=>{
                                        this.setState({email:e.target.value})
                                    }
                                    } value={this.state.email}/>
                                    <br/>
                                    <input type="password" className="form-control" name="password" style={{height: "2.7rem"}} placeholder={"****"} onChange={e=>{
                                        this.setState({password:e.target.value})
                                    }
                                    } value={this.state.password}/>
                                    <br/>
                                    <a href="#!" onClick={()=>{
                                        this.setState({login:false});
                                    }}>Signup instead</a>
                                    <br/>
                                    <div style={{width:"100%"}}>
                                        <ActionButton text="Authentify Me" loading={this.state.loadingLogin} style={{float:"right"}} onClick={()=>{
                                            this.setState({loadingLogin:true});
                                            get('/user/login',this.state,(resp)=>{
                                                this.setState({loadingLogin:false});
                                                window.localStorage.setItem("authToken",resp.data);
                                                this.props.history.push('/feed');
                                            },()=>{
                                                this.setState({loadingLogin:false});
                                            });
                                        }}/>
                                    </div>
                                </div>}
                                {!this.state.login && <div className="signup-content">
                                    <h1 className="sketch" style={{color:"black",fontSize:"4em",textStyle:"italic"}}>Sign Up</h1>
                                    <br/>
                                    <input type="text" className="form-control" name="name" style={{height: "2.7rem"}} placeholder="Your name here" onChange={e=>{
                                        this.setState({name:e.target.value})
                                    }
                                    } value={this.state.name}/>
                                    <br/>
                                    <input type="text" className="form-control" name="email" style={{height: "2.7rem"}} placeholder="Your email here" onChange={e=>{
                                        this.setState({email:e.target.value})
                                    }
                                    } value={this.state.email}/>
                                    <br/>
                                    <input type="password" className="form-control" name="password" style={{height: "2.7rem"}} placeholder={"****"} onChange={e=>{
                                        this.setState({password:e.target.value})
                                    }
                                    } value={this.state.password}/>
                                    <br/>
                                    <a href="#!" onClick={()=>{
                                        this.setState({login:true});
                                    }}>Login instead</a>
                                    <br/>
                                    <div style={{width:"100%"}}>
                                        <ActionButton text="Create Account" loading={this.state.loadingLogin} style={{float:"right"}} onClick={()=>{
                                            this.setState({loadingLogin:true});
                                            get('/user/create',this.state,(resp)=>{
                                                this.setState({loadingLogin:false});
                                                window.localStorage.setItem("authToken",resp.data)
                                                this.props.history.push('/feed');
                                            },()=>{
                                                this.setState({loadingLogin:false});
                                            });
                                        }}/>
                                    </div>
                                </div>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default withRouter(Welcome);