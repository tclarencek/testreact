import React from 'react';
import {withRouter} from 'react-router-dom';
import PostPreview from '../components/PostPreview';
import Header from "../components/Header";
import './Article.css';
import {  Editor} from 'react-draft-wysiwyg';
import {EditorState,
    ContentState,
    convertFromHTML,
    CompositeDecorator,
    convertToRaw,
    getDefaultKeyBinding} from 'draft-js';
import {stateToHTML} from 'draft-js-export-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import ActionButton from "../components/ActionButton";
import Comment from "../components/Comment";
import {get,url,post} from '../utils/Network';
class Article extends React.Component {
    state={
        post:{},
        loading:true,
        comments:[],
        similar:[],
        sendingComment:false,
        editorState:EditorState.createEmpty()
    };
    componentDidMount(){
        console.log(this.props.match.params.id)
        get('/post/'+this.props.match.params.id,{},(post)=>{
            post=post.data;
            console.log(post)
            this.setState({post});
            get('/post/'+this.props.match.params.id+'/comment/all',{},(comments)=>{
                comments =comments.data;
                this.setState({comments});
                get('/category/'+post.categoryId+'/posts',{},(similar)=>{
                    similar = similar.data;
                    let sm = []
                    similar.forEach(sim=>{
                        if(sim._id!==this.props.match.params.id){
                            sm.push(sim)
                        }
                    })
                    this.setState({similar:sm});
                },()=>{this.setState({loading:false})});
            },()=>{});

        },()=>{

        })
    }

    render() {
        return (
           <div>
               {this.state.post &&  <div>
                   <Header content={["This is the post"]} image={this.state.post.postImage} context={this}/>
                   <div className="container" style={{position: "relative",
                       top: -34}}>
                       <div className="post-module">
                           <h6>By <i class="fa fa-user"/>&nbsp;{this.state.post.userId}</h6>
                           <hr/>
                           <h3 style={{color: "#05a081"}}>{this.state.post.title}</h3>
                           <div dangerouslySetInnerHTML={{__html:this.state.post.content}}>

                           </div>
                           <br/>
                           <h6><i className="fa fa-clock"/> {this.state.post.created_at}</h6>
                           <br/>
                           <br/>
                           <h6><i class="fas fa-comments"/>&nbsp;Comments</h6>
                           <hr/>
                           <br/>
                           {this.state.comments.map(elt=>{
                               return(
                                   <Comment at={elt.created_at} content={elt.comment} author={elt.userId}/>
                               )
                           })}

                           <br/>
                           <div style={{border:"1px solid rgba(0,0,0,0.1)"}}>
                               <Editor
                                   toolbarClassName="toolbarClassName"
                                   wrapperClassName="wrapperClassName"
                                   editorClassName="editorClassName"
                                   onEditorStateChange={(editorState)=>{
                                       this.setState({editorState})

                                   }}
                                   onChange={(e)=>{}}
                               />


                           </div>
                           <br/>
                           <ActionButton text="Add Comment" style={{float:"right"}} loading={this.state.sendingComment}  onClick={()=>{
                               this.setState({sendingComment:true});
                               post(`/post/${this.props.match.params.id}/comment/create`,{comment:stateToHTML(this.state.editorState.getCurrentContent())},(resp)=>{
                                  let comments =  this.state.comments;
                                       comments.push(resp.data);
                                   this.setState({comments});
                                   this.setState({sendingComment:false});
                               },()=>{
                                   this.setState({sendingComment:false});
                               });
                           }}/>
                           <br/>
                           <br/>
                           <br/>
                       </div>
                   </div>


                   <br/>

                   <div className="container">
                       <br/>
                       <center><h1>Similar Posts</h1>
                           <h5>{this.state.similar.length} Post(s) from the same category</h5></center>
                       <br/>

                       <hr style={{marginBottom: 0}}/>
                       {this.state.loading && !this.state.similar &&
                       <div>
                           <br/>
                           <center>
                               <i className="fa fa-spinner fa-spin" style={{fontSize:"4em"}}/>
                           </center>
                       </div>}
                       {!this.state.loading && this.state.similar &&
                       <div className="row">
                           {this.state.similar.map((elt,i)=>{
                               if(i<3){
                                   return(
                                       <div className="col-sm-12 col-md-4">
                                           <PostPreview title={elt.title} key={i} author={elt.userId} excerpt={elt.excerpt} image={elt.postImage?elt.postImage:"https://www.pexels.com/photo/4458/download/?search_query=blog&tracking_id=yg0e5xhzzf"} onClick={()=>{
                                               window.location = ('/article/'+elt._id)
                                           }}/>
                                       </div>
                                   )
                               }
                           })}
                       </div>
                       }

                   </div>
               </div>}
               {!this.state.post && this.state.loading &&
               <div>
                   <br/>
                   <center>
                       <i className="fa fa-spinner fa-spin" style={{fontSize:"4em"}}/>
                   </center>
               </div>}
           </div>
        );
    }
}

export default withRouter(Article);