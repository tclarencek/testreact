import React from 'react';
import {withRouter} from 'react-router-dom';
import PostPreview from '../components/PostPreview';
import Header from "../components/Header";
import { url} from '../utils/Network';
import {Editor} from 'react-draft-wysiwyg';
import axios from 'axios';
import {
    EditorState,
    ContentState,
    convertFromHTML,
    CompositeDecorator,
    convertToRaw,
    getDefaultKeyBinding
} from 'draft-js';
import ImageUploader from 'react-images-upload';
import {stateToHTML} from 'draft-js-export-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import ActionButton from "../components/ActionButton";
import {toast} from 'react-toastify';

class NewPost extends React.Component {
    constructor(props) {
        super(props);
        this.onDrop = this.onDrop.bind(this);
    }

    state = {
        loading: true, content: [], pictures: [],
        category: 1,
        excerpt: '',
        sendingPost:false
    };

    componentDidMount() {
        if (this.props.type === 1) {

        }
        if (this.props.type === 0) {

        }
    }

    upload() {

        console.log(this.state.pictures);
        return;


    }

    onDrop(picture) {
        console.log(picture);


        this.setState({
            pictures: this.state.pictures.concat(picture),
        });
    }

    render() {
        return (
            <div>
                <Header content={["Create a new post"]} context={this}/>

                <br/>

                <div className="container">
                    <h1>Fill in the form below </h1>
                    <br/>
                    <input type="text" name="title" value={this.state.title} onChange={(e) => {
                        this.setState({title: e.target.value})
                    }} className="form-control" placeholder="The post title"/>
                    <br/>
                    <input type="text" name="excerpt" value={this.state.excerpt} onChange={(e) => {
                        this.setState({excerpt: e.target.value})
                    }} className="form-control" placeholder="The pos excerpt" max="150"/>
                    <br/>
                    <select className="form-control" value={this.state.category} onChange={(e) => {
                        this.setState({category: e.target.value})
                    }}>
                        <option value="1">Fashion</option>
                        <option value="2">Finance</option>
                        <option value="3">Technology</option>
                        <option value="4">Sport</option>
                        <option value="5">Health</option>

                    </select>
                    <br/>
                    <img/>
                    <ImageUploader
                        withIcon={true}
                        buttonText='Choose images'
                        onChange={this.onDrop}
                        imgExtension={['.jpg', '.gif', '.png', '.gif']}
                        maxFileSize={5242880}
                        withPreview={true}
                    />


                    <br/>
                    <div style={{border: "1px solid rgba(0,0,0,0.1)"}}>
                        <Editor
                            toolbarClassName="toolbarClassName"
                            wrapperClassName="wrapperClassName"
                            editorClassName="editorClassName"
                            onEditorStateChange={(editorState) => {
                                this.setState({editorState})

                            }}
                            onChange={(e) => {
                            }}
                        />


                    </div>
                    <br/>
                    <ActionButton text="Add Post" style={{float: "right"}} loading={this.state.sendingPost}
                                  onClick={() => {
                                      const context =this;
                                      this.setState({sendingPost: true});
                                      // let data = new FormData();
                                      // // data.append('image', this.state.pictures[0]);
                                      // data.append('title', this.state.title);
                                      // data.append('excerpt', this.state.excerpt);
                                      // data.append('categoryId', this.state.category);
                                      // data.append('content', stateToHTML(this.state.editorState.getCurrentContent()));
                                      let s ; let image =null;
                                      if( s = document.getElementsByClassName("uploadPicture")[0]){
                                        image = s.src
                                      }

                                      axios.defaults.headers.common['Authorization'] = 'Bearer ' + window.localStorage.getItem('authToken');
                                      // console.log(data);
                                      axios.post(url()+'/post/create', {
                                        title:this.state.title,
                                          excerpt:this.state.excerpt,
                                          categoryId:this.state.category,
                                          content:stateToHTML(this.state.editorState.getCurrentContent()),
                                          image,
                                      })
                                          .then(r => {
                                              console.log(r.data)
                                              context.props.history.push("/article/"+r.data.data._id)
                                          })
                                          .catch(e => {
                                              try{
                                                  toast(e.response.data.data)
                                              }catch (e){
                                                  toast("Can't reach server")
                                              }
                                          }).finally(() => this.setState({sendingPost: false}))
                                  }}/>
                </div>
            </div>
        );
    }
}

export default withRouter(NewPost);