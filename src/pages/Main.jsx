import React from "react";
import Typed from "react-typed";
import {withRouter} from 'react-router-dom';
 // import ReactCardCarousel from 'react-card-carousel';
import CategoryPreview from "../components/CategoryPreview";
import './Main.css';
import Header from '../components/Header';
import PostPreview from "../components/PostPreview";
import {get,url} from '../utils/Network';
import ActionButton from "../components/ActionButton";
class Main extends React.Component{
    state={
      fashion:[],
      finance  :[],
      tech  :[],
      sport  :[],
      health  :[],
        loaded:0,

    };
    componentDidMount(

    ){
        get('/category/1/posts',{},(res)=>{
            this.setState({fashion:res.data,loaded:this.state.loaded++});

        },()=>{})
        get('/category/2/posts',{},(res)=>{
            this.setState({finance:res.data,loaded:this.state.loaded++});
        },()=>{})
        get('/category/3/posts',{},(res)=>{
            this.setState({tech:res.data,loaded:this.state.loaded++});
        },()=>{})
        get('/category/4/posts',{},(res)=>{
            this.setState({sport:res.data,loaded:this.state.loaded++});
        },()=>{})
        get('/category/5/posts',{},(res)=>{
            this.setState({health:res.data,loaded:this.state.loaded++});
        },()=>{})

    }
    render(){
        return(
          <div>

              <Header content={['Welcome! You are at the homepage','Of the blog ^_^','Scroll down to view the content','View the full content <a href="/posts">HERE</a>'] } context={this}/>
              <div style={{padding:"0 3%"}}>
                  <br/>
                 <center><h1>Categories</h1>
                 <h5>These are the different categories present</h5></center>
                    <br/>
                  <div className="row">
                     <div className="col-md-8">
                         <CategoryPreview image="/categories/fashion.jpg" title="Fashion" onClick={()=>{
                             this.props.history.push('/category/1')
                         }}/>
                     </div>
                     <div className="col-md-4">
                         <CategoryPreview image="/categories/finance.jpg" title="Finance" onClick={()=>{
                             this.props.history.push('/category/2')
                         }}/>
                     </div>
                     <div className="col-md-4" style={{top:30,position:"relative"}}>
                         <CategoryPreview image="/categories/tech.jpg" title="Technology" onClick={()=>{
                             this.props.history.push('/category/3')
                         }}/>
                     </div>
                     <div className="col-md-4" style={{top:30,position:"relative"}}>
                         <CategoryPreview image="/categories/sport.jpg" title="Sports" onClick={()=>{
                             this.props.history.push('/category/4')
                         }}/>
                     </div>
                     <div className="col-md-4" style={{top:30,position:"relative"}}>
                         <CategoryPreview image="/categories/health.jpg" title="Health" onClick={()=>{
                             this.props.history.push('/category/5')
                         }}/>
                     </div>
                  </div>
              </div>
              <div>

                <center>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <h3>You can click here to create a post</h3>
                    <br/>
                    <ActionButton text="Create post now" onClick={()=>{
                        this.props.history.push("/new/post")
                    }}/>
                </center>
              </div>
              <br/>
              <br/>
              <br/>
              <div className="row gg" style={{margin:0}}>
                  <div className="col-sm-12 col-md-5" style={{backgroundColor:"#05a081",padding:50}}>
                      <div className="white-section-cover align-self-center" style={{width:"100%"}}>
                          <div className="white-section"  style={{height:"100%"}}>
                              <Typed
                                  strings={['This is the seperator<br/>of a different section']}
                                  typeSpeed={40}
                                  className="main_text"
                              />
                          </div>
                      </div>
                  </div>
                  <div className="col-sm-12 col-md-7 d-flex flex-column justify-content-center pl-5" style={{backgroundColor:"rgba(0,0,0,0.03)",minHeight:300}}>
                      <h1>Now, Look at this!</h1>
                      <h4>This section regroups posts per categories</h4>
                  </div>
              </div>
              <br/>

             <div className="container">
                 <br/>
                 <center><h1>Most Recent</h1>
                     <h5>These are the most recent most by category</h5></center>
                 <br/>
                 <h6>Fashion</h6>
                 <hr style={{marginBottom:0}}/>
                 <div className="row">
                     {this.state.fashion.map((elt,i)=>{
                         if(i<3){
                             return(
                                 <div className="col-sm-12 col-md-4">
                                     <PostPreview title={elt.title} key={i} author={elt.userId} excerpt={elt.excerpt} image={elt.postImage?elt.postImage:"https://www.pexels.com/photo/4458/download/?search_query=blog&tracking_id=yg0e5xhzzf"} onClick={()=>{
                                         this.props.history.push('/article/'+elt._id)
                                     }}/>
                                 </div>
                             )
                         }
                     })}
                 </div>                 <br/>

                 <h6>Finance</h6>
                 <hr style={{marginBottom:0}}/>
                 <div className="row">
                     {this.state.finance.map((elt,i)=>{
                         if(i<3){
                             return(
                                 <div className="col-sm-12 col-md-4">
                                     <PostPreview title={elt.title} key={i} author={elt.userId} excerpt={elt.excerpt} image={elt.postImage?elt.postImage:"https://www.pexels.com/photo/4458/download/?search_query=blog&tracking_id=yg0e5xhzzf"}/>
                                 </div>
                             )
                         }
                     })}
                 </div>
                 <br/>
                 <h6>Technology</h6>
                 <hr style={{marginBottom:0}}/>
                 <div className="row">
                     {this.state.tech.map((elt,i)=>{
                         if(i<3){
                             return(
                                 <div className="col-sm-12 col-md-4">
                                     <PostPreview title={elt.title} key={i} author={elt.userId} excerpt={elt.excerpt} image={elt.postImage?elt.postImage:"https://www.pexels.com/photo/4458/download/?search_query=blog&tracking_id=yg0e5xhzzf"} onClick={()=>{
                                         this.props.history.push('/article/'+elt._id)
                                     }}/>
                                 </div>
                             )
                         }
                     })}
                 </div>
                 <br/>
                 <h6>Sports</h6>
                 <hr style={{marginBottom:0}}/>
                 <div className="row">
                     {this.state.sport.map((elt,i)=>{
                         if(i<3){
                             return(
                                 <div className="col-sm-12 col-md-4">
                                     <PostPreview title={elt.title} key={i} author={elt.userId} excerpt={elt.excerpt} image={elt.postImage?elt.postImage:"https://www.pexels.com/photo/4458/download/?search_query=blog&tracking_id=yg0e5xhzzf"} onClick={()=>{
                                         this.props.history.push('/article/'+elt._id)
                                     }}/>
                                 </div>
                             )
                         }
                     })}
                 </div>
                 <br/>
                 <h6>Health</h6>
                 <hr style={{marginBottom:0}}/>
                 <div className="row">
                     {this.state.health.map((elt,i)=>{
                         if(i<3){
                             return(
                                 <div className="col-sm-12 col-md-4">
                                     <PostPreview title={elt.title} key={i} author={elt.userId} excerpt={elt.excerpt} image={elt.postImage?elt.postImage:"https://www.pexels.com/photo/4458/download/?search_query=blog&tracking_id=yg0e5xhzzf"} onClick={()=>{
                                         this.props.history.push('/article/'+elt._id)
                                     }}/>
                                 </div>
                             )
                         }
                     })}
                 </div>
                 <br/>
             </div>
              <br/>
              <div className="row" style={{margin:0}}>

                  <div className="col-sm-12 d-flex flex-column justify-content-center p-5" style={{backgroundColor:"rgba(0,0,0,0.03)"}}>
                     <center> <h1>Now, Look at the client side 404 !</h1>
                         <br/>
                         <ActionButton text="View 404" onClick={()=>{
                             this.props.history.push("/asd39qejdd")
                         }}/>
                         </center>
                  </div>
              </div>
              <div style={{width:"100%",backgroundColor:"#05a081",padding:"30px 0"}} className="d-flex flex-column justify-content-center align-items-center align-content-center">
                  <div className="white-section-cover align-self-center" style={{width:"50%"}}>
                      <div className="white-section d-flex align-content-center justify-content-center" >
                          <Typed
                              strings={['You can view the full blog <a href="/posts">HERE</a>']}
                              typeSpeed={40}
                              className="main_text"
                          />
                      </div>
                  </div>
              </div>

          </div>
        );
    }
    

}
export default withRouter(Main);