import React from 'react';

export default function Comment({at,content,author}) {
    return (
        <div class="row" style={{marginBottom:10}}>
            <div className="col-sm-12 col-md-1">
                <center><i className="fa fa-comment font-weight-bolder" style={{fontSize: "3em"}}>
                </i></center>
            </div>
            <div className="col-sm-12 col-md-11">
                <small><i class="fa fa-user"/>&nbsp;{author}</small><br/>
                <small><i class="fa fa-clock"/>&nbsp;{at}</small><br/>
                <small dangerouslySetInnerHTML={{__html:content}}>


                </small> <hr/>
            </div>
        </div>
    );
}