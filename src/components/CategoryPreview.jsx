import React from "react";
import './CategoryPreview.css';
export default function CategoryPreview({title,image,...rest}){
    return(
        <div {...rest} className="the-container" style={{backgroundImage:"url('"+image+"')"}} >
            <div className="the-container-cover">
                <h4 style={{color:"white"}}>{title}</h4>
            </div>
        </div>
    )
};