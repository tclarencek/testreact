import React from 'react';
import '../pages/Main.css';

import Typed from "react-typed";
export default function({content,image,context}){
    return(
        <div>
            <div className="p-3">
               <span className="float-right" style={{cursor:"pointer"}} onClick={()=>{
                   window.localStorage.removeItem("authToken")
                   window.location='/'
               }}><i class="fa fa-user"></i> Logout</span>
                <br/>
            </div>
            <div style={{height: 285, backgroundImage:`${image?image:'url("https://www.pexels.com/photo/4458/download/?search_query=blog&tracking_id=yg0e5xhzzf")'}`
            }} className="back">
                <div style={{width:"100%",height:"100%",padding:40,background:"rgba(0,0,0,0.3)"}} className=" d-flex align-content-center justify-content-center">
                    {/*<h1 className="sketch" style={{color:"white",fontSize:"5em",textStyle:"italic"}}>Ze Blog</h1>*/}
                    <div className="white-section-cover align-self-center" >
                        <div className="white-section d-flex align-content-center justify-content-center" >
                            <Typed
                                strings={content}
                                typeSpeed={40}
                                className="main_text"
                            />
                        </div>
                    </div>
                </div>


            </div>
        </div>
    )
}