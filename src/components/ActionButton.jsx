import React from "react";
import './ActionButton.css';
export default function ActionButton({text, loading=false ,...rest}) {

    return (
        <button  className="button" {...rest}>{loading && <i className="fa fa-spinner fa-spin"></i>} {loading &&
        <span>Please Wait...</span>} {!loading && <span>{text}</span>}</button>
    )
}