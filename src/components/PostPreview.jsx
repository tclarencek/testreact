import React from 'react';
import './PostPreview.css';
export default function PostPreview({title,excerpt,date,author,image,...rest}){
    return(
        <div className="covering" style={{backgroundImage: `url("${image}")`}} {...rest}>
            <div class="covering-back">
                <span style={{color:"white"}}><i class="fa fa-user"/>&nbsp;{author}</span>
                <h5 style={{color:"rgb(5, 160, 129)",fontWeight:"bolder"}}>{title}</h5>
                <p style={{color:"white"}}>{excerpt}</p>
            </div>
        </div>
    )
}