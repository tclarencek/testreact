import axios from 'axios';
import {toast} from 'react-toastify';
let baseUrl ='http://206.189.27.169:5000';

export function url(){
    return baseUrl;
}
export function get(link,params,successCallback,finallyCallback){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + window.localStorage.getItem('authToken');
    // axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8'
    axios.get(baseUrl+link,{params,headers: {
        Authorization: 'Bearer ' + window.localStorage.getItem('authToken')
    }}).then(response=>{
        console.log(response)
        successCallback(response.data);
    }).catch(function (p1) {
        try {
            toast(p1.response.data.data);
        }catch (e) {
            console.error(e);
            toast("Can't reach server");}
    }).finally(()=>{
       try{ finallyCallback()}catch (e){

       }
    })
}

export  function post(link,params,successCallback,finallyCallback){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + window.localStorage.getItem('authToken');
    axios.post(baseUrl+link,params).then(response=>{
        successCallback(response.data);
    }).catch(function (p1) {
        try {
            toast(p1.response.data.data);
        }catch (e) {
            console.error(e);
            toast("Can't reach server");
        }
    }).finally(()=>{
        try{ finallyCallback()}catch (e){

        }
    })
}