import React from 'react';
import logo from './logo.svg';
import './App.css';
import './bootstrap.css';
import Main from "./pages/Main";
import Welcome from "./pages/welcome";
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import Blog from "./pages/Blog";
import NewPost from "./pages/NewPost";
import Article from "./pages/Article";
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainer,toast} from 'react-toastify';
function App() {
    let isAuthenticated =
    window.localStorage.getItem("authToken")
  return (
    <Router>
        <ToastContainer/>
        {!isAuthenticated && <Switch>
        <Route exact path="/">
            <Welcome/>
        </Route>
        <Route path="*"  exact component={err_404}/>
    </Switch>}
        {isAuthenticated && <Switch>
        
        <Route exact path="/">
            <Main/>
        </Route>
        <Route exact path="/feed">
            <Main/>
        </Route>
        <Route exact path="/posts">
            <Blog type={0}/>
        </Route>
        <Route exact path="/article/:id">
            <Article/>
        </Route>
        <Route exact path="/category/:id">
            <Blog type={1}/>
        </Route>
        <Route exact path="/new/post">
            <NewPost type={1}/>
        </Route>
        <Route path="*"  exact component={err_404}/>
    </Switch>}



    </Router>
  );
}
function err_404(){
    return(
        <div>
            <center>
                <br/>
                <br/>
                <img src={require('./assets/404.gif')}/>
            </center>
        </div>
    )
}
export default App;
