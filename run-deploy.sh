#!/usr/bin/env bash
CI_JOB_TOKEN="$1"
CI_REGISTRY="$2"
CI_PROJECT_PATH="$3"

#if you wanted to use them in the docker-compose for example, "export CI_JOB_TOKEN" so it becomes an env
# clone repo
cd /var/test/test-node
# project resides in shoppy-node/shoppy-node
# rm -rf shoppy-node
git pull "https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git" master

npm install -g pm2
#cd shoppy-node
npm install



npm run start
